# django-tutorial

Tutorial from [Django documentation](https://docs.djangoproject.com/en/3.0/)

This repo includes the public site that lets people view polls and vote in them and the admin site that lets you add, change, and delete polls.

## Prerequisite

### Create a virtual environment for the project

```zsh
# macOS/Linux
python3 -m venv .venv

# Windows
python -m venv .venv
```

### Install Django

The easiest way is to install an official release of Django with pip

```zsh
python -m pip install Django
```

## Usage

### Run the development server

Go into the mysite directory and run the following commands:

```zsh
❯ python manage.py runserver
```

The server will start at <http://127.0.0.1:8000/>

### Database setup

The database is configured by Django settings defined in the `mysite/settings.py` module.

To create the tables used by apps defined in the `INSTALLED_APPS` list, run:

```zsh
❯ python manage.py migrate
```

Using SQLite, this command will create `db.sqlite3` into the project root directory. You can run the following command to see the created schema:

```zsh
❯ sqlite3 db.sqlite3
❯ .schema
```

When making new models or applying some changes to your models, run the comand `makemigrations` to create migration scripts for those changes; the run `migrate` to apply those changes to the database.

For example, after creating the two models `Question` and `Choice` in the polls app, running the following command creates `polls\migrations\0001_initial.py`

```zsh
❯ python manage.py makemigrations polls
```

Then you can run the `migrate` command to apply the changes.

## Administration

Django comes with the authentication framework `django.contrib.auth`.

### Create an admin user

Run the following command:

```zsh
python manage.py createsuperuser
```

To see the admin\'s login screen:

1. start the server with `python manage.py runserver`
2. open a Web browser
3. go to <http://127.0.0.1:8000/admin/>.
